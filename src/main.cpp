/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/11 15:36:30 by lcharbon          #+#    #+#             */
/*   Updated: 2019/03/18 17:57:39 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "computorv2.hpp"

string				cmp_treat(string line)
{
	string			op_left;
	string			op_right;
	string			*op;
	string			result = "";

	if (line.find("=") == string::npos)
		return ("Erreur, aucun '='\n");
	if (count(line.begin(), line.end(), '=') > 1)
		return ("Erreur, plusieurs '='\n");
	op = parser(line);
	op_left = op[0];
	op_right = op[1];

	if (op_right.find("?") != string::npos && op_right.length() > 1)
	{
		delete[] op;
		return (op_right.append(" : Syntax error\n"));
	}

	if (op_right.find("?") != string::npos)
		result = cmp_calculate(op_left);
	else
		result = cmp_assignation(op_left, op_right);
	delete[] op;
	return (result);
}

int					main(void)
{
	string			line;
	string			result;

	cout << "computorv2>>>";
	line = "";
	while (getline(cin, line))
	{
		if (!line.find("exit") || !line.find("quit") || !line.find("leave"))
			break ;
		if (!line.empty())
			result = cmp_treat(line);
		else
			result = "";
		if (result != "")
			cout << result << "\n";
		cout << "computorv2>>>";
	}
	return 0;
}
