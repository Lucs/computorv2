/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculate_rpn.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/14 12:35:42 by lcharbon          #+#    #+#             */
/*   Updated: 2019/03/21 17:27:37 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "computorv2.hpp"

float				ft_powf(float base, float exponent)
{
	float			b;

	b = base;
	while (exponent > 1)
	{
		base *= b;
		exponent--;
	}
	return (base);
}

string				ft_create_strange_string(string a, string b, char c)
{
	b.append(" ");
	b.append(1, c);
	b.append(" ");
	b.append(a);
	return (b);
}

string				ft_multiply_complex(string val1, string val2)
{
	unsigned char	mim1 = 0;
	unsigned char	im1 = 0;
	unsigned char	mim2 = 0;
	unsigned char	im2 = 0;

	if (ft_is_complex(val1, val1))
		im1 = 1;
	if (val1.find("-") != string::npos)
		mim1 = 1;
	if (ft_is_complex(val2, val2))
		im2 = 1;
	if (val2.find("-") != string::npos)
		mim2 = 1;

	if (im1 == 0)
	{
		if (mim2 == 0)
			return ("i");
		else
			return ("-i");
	}
	if (im2 == 0)
	{
		if (mim1 == 0)
			return ("i");
		else
			return ("-i");
	}
	if (mim1 == 0 && mim2 == 0)
		return ("-1");
	if (mim1 == 0 && mim2 == 1)
		return ("1");
	if (mim1 == 1 && mim2 == 0)
		return ("1");
	return ("ERROR");
}

string				ft_operate_complex(string val1, string val2, char c)
{
	string			ret;
	string			imagin;
	float			res;
	float			a;
	float			b;

	b = atof(val1.c_str());
	a = atof(val2.c_str());

	res = 0;
	imagin = "i";
	if (ft_is_complex(val1, val1) && b == 0)
		b = 1;
	if (ft_is_complex(val2, val2) && a == 0)
		a = 1;
	if (c == '+')
		res = a + b;
	else if (c == '-')
		res = a - b;
	else if (c == '*')
	{
		res = a * b;
		imagin = ft_multiply_complex(val1, val2);
		if (imagin.compare("-i") == 0)
		{
			res *= -1;
			imagin = "i";
		}
		if (imagin.compare("-1") == 0)
		{
			res *= -1;
			imagin = "";
		}
		if (imagin.compare("1") == 0)
			imagin = "";
	}
	else if (c == '/')
		res = a / b;
	else if (c == '^')
		res = ft_powf(a, b);
	//           IMAGINARY CALCUL          //
	return (to_string(res).append(imagin));
}

string				calculate_rpn(string rpn)
{
	string			ret;
	float			res;
	stack<string>	var;
	stack<string>	ncalc;
	stack<float>	stack_num;
	stack<char>		stack_op;
	int				i;
	unsigned char	inc;
	string			tmp;
	string			val1 = "";
	string			val2 = "";
	char			c = 0;

	int	e;

	cout << rpn << "\n";
	i = 0;
	string rien = "";
	while (rpn[i])
	{
		rien.clear();
		inc = 0;
		if (var.size() >= 2 && stack_op.size() >= 1)
		{
			tmp.clear();
			c = stack_op.top();
			stack_op.pop();
			val2 = var.top();
			var.pop();
			val1 = var.top();
			var.pop();
		cout << val1 << " | " << val2 << "\n";
			if (!ft_is_matrice(val1, val2) && !ft_is_complex(val1, val2) && val1.find("NOPE") == string::npos && val2.find("NOPE") == string::npos)
			{
				if (c == '+')
					res = atof(val1.c_str()) + atof(val2.c_str());
				else if (c == '-')
					res = atof(val1.c_str()) - atof(val2.c_str());
				else if (c == '*')
					res = atof(val1.c_str()) * atof(val2.c_str());
				else if (c == '/')
					res = atof(val1.c_str()) / atof(val2.c_str());
				else if (c == '^')
					res = ft_powf(atof(val1.c_str()), atof(val2.c_str()));
				tmp = to_string(res);
				var.push(tmp);
			}
			else if (ft_is_matrice(val1, val2))
			{
				tmp = ft_operate_matrice(val1, val2, c);
				var.push(tmp);
			}
			else if (ft_is_complex(val1, val2))
			{
				tmp = ft_operate_complex(val1, val2, c);
				var.push(tmp);
			}
			else
			{
				tmp = ft_create_strange_string(val1, val2, c);
				ncalc.push(tmp);
				var.push("NOPE");
			}
			tmp.clear();
		}
		if ((rpn[i] != ' ' && !ft_issign(rpn[i]) && rpn[i] != '[') ||
		(rpn[i] == '-' && rpn[i + 1] && (isdigit(rpn[i + 1]) || rpn[i + 1] == 'i')))
		{
			e = i;
			while (rpn[i] && rpn[i] != ' ')
			{
				inc = 1;
				i++;
			}
			rien.append(&rpn[e], 0, i - e);
			var.push(rien);
		}
		if (inc)
		{
			inc = 0;
			i--;
		}
		else if (ft_issign(rpn[i]))
			stack_op.push(rpn[i]);
		else if (rpn[i] == '[')
		{
			int	m = 1;
			e = i;
			i++;
			while (rpn[i] && m > 0)
			{
				if (rpn[i] == ']')
					m--;
				else if (rpn[i] == '[')
					m++;
				inc = 1;
				i++;
			}
			i++;
			rien.append(&rpn[e], 0, i - e);
			var.push(rien);
		}
		if (inc)
			i--;
		i++;
	}
	ret = var.top();
//	while (ncalc.size() > 0)
//	{
//		cout << ncalc.top() << " ";
//		ncalc.pop();
//	}
//	cout << ret << "\n";
	var.pop();
	return (ret);
}

/*
int			main(void)
{
	string str = "3 7550 * i * 3 x * + 5 [ 1 , 2 ] * -";
//	string str = "3 7550 * i * 3 x * + 5 -";
	float ret;

	ret = calculate_rpn(str);
	return (1);
}

*/
