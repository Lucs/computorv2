/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Shunting_Yard_algorithme.cpp                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/16 16:19:36 by lcharbon          #+#    #+#             */
/*   Updated: 2019/03/21 15:44:53 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "computorv2.hpp"

typedef struct		s_rpn
{
	string			s;
	int				pre;
	unsigned char	r;
}					t_rpn;

t_rpn				*get_operator(string s, t_rpn dictio[5])
{
	for (int i = 0; i < 5; i++)
		if (s == dictio[i].s)
			return (&dictio[i]);
	return (NULL);
}

string					to_rpn(string str)
{
	t_rpn				dictio[5]; 
	string				token[500];
	int					i = 0;
	t_rpn				*operate = NULL;
	t_rpn				*operate2 = NULL;
	stack<string>		stacko;
	stack<string>		output;

	dictio[0].s = "^";
	dictio[0].pre = 4;
	dictio[0].r = 1;

	dictio[1].s = "*";
	dictio[1].pre = 3;
	dictio[1].r = 0;

	dictio[2].s = "/";
	dictio[2].pre = 3;
	dictio[2].r = 0;

	dictio[3].s = "+";
	dictio[3].pre = 2;
	dictio[3].r = 0;

	dictio[4].s = "-";
	dictio[4].pre = 2;
	dictio[4].r = 0;

	string ret = "";
	string	delim = " ";
	size_t	pos = 0;
	int		tr = 0;
	while ((pos = str.find(delim)) != string::npos)
	{
		token[tr] = str.substr(0, pos);
		str.erase(0, pos + delim.length());
		tr++;
	}
	token[tr] = str;
	tr++;
	while (i < tr)
	{
		char eb = token[i][0];
		if ((eb != '+' && eb != '-' && eb != '/' &&eb != '*' &&eb != '(' &&eb != ')' && eb != '^')
		|| (eb == '-' && strlen(token[i].c_str()) > 1 && (isdigit(token[i][1]) || token[i][1] == 'i')))
			output.push(token[i]);
		else if ((operate = get_operator(token[i], dictio)) != NULL)
		{
			while (stacko.size() > 0 && (operate2 = get_operator(stacko.top(), dictio)) != NULL)
			{
				int c = operate->pre - operate2->pre;
				if (c < 0 || (operate->r == 0 && c <= 0))
				{
					output.push(stacko.top());
					stacko.pop();
				}
				else
					break ;
			}
			stacko.push(token[i]);
		}
		else if (token[i][0] == '(')
			stacko.push(token[i]);
		else if (token[i][0] == ')')
		{
			string top = "";
			while (stacko.size() > 0 && ((top = stacko.top()) != "("))
			{
				output.push(stacko.top());
				stacko.pop();
			}
			stacko.pop();
			if (top != "(")
				return (ret);
		}
		i++;
	}
	while (stacko.size() > 0)
	{
		string top = stacko.top();
		stacko.pop();
		if ((top != "+" && top != "-" && top != "*" && top != "/" && top != "^"))
			return (ret);
		output.push(top);
	}
	string abc;
	stack<string> efd;
	while (output.size() > 0)
	{
		efd.push(output.top());
		output.pop();
	}
	while (efd.size() > 0)
	{
		ret.append(efd.top());
		ret.append(" ");
		efd.pop();
	}
	return (ret);
}
/*
int		main(void)
{
	string tests[] = { 
		"( 3 * 7550 * i ) + 3 * x - 5 * [ 1 , 2 ]",
		"( varA * varB ) + f ( 2 )  * [ 1, 2 ]",
		"( vara + varb ) * varc",
		"( ( ( 9 - 7 ) * ( 3 + 4 ) * 4 ) + 9 ) + 3",
		"3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3",
		"123",
		"3+4 * 2 / ( 1 - 5 ) ^ 2 ^ 3.14",
		"(((((((1+2+3**(4 + 5))))))",
		"a^(b + c/d * .1e5)!",
		"(1**2)**3",
		"2 + 2 *",
	};
	string	res;

	int a = 0;
	while (a < 7)
	{
		res = to_rpn(tests[a]);
		int i = -1;
		while (res[++i] != 0)
			write(1, &res[i], 1);
		write(1, "\n", 1);
		a++;
	}
	return (1);
}*/
