/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abassibe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/11 17:43:48 by abassibe          #+#    #+#             */
/*   Updated: 2019/03/21 14:03:19 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

std::string		*parser(std::string str)
{
	std::string		*array = new std::string[2];

//	str.erase(remove_if(str.begin(), str.end(), isspace), str.end());
	array[0] = str.substr(0, str.find("="));
	array[1] = str.substr(str.find("=") + 1, str.size() - str.find("=") - 1);
	return (array);
}
