/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculate.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/12 14:01:09 by lcharbon          #+#    #+#             */
/*   Updated: 2019/03/21 17:28:53 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "computorv2.hpp"

unsigned char	need_insert_multiply(string base)
{
	int			i;

	i = 0;
	while (isdigit(base[i]))
		i++;
	if (base[i] == 'i')
		return (1);
	return (0);
}

string			ft_insert_space(string base)
{
	string	ret;
	int		i;

	i = 0;

	ret = "";
	while (base[i] != 0)
	{
		if (base[i] == ' ')
		{
			i++;
			continue ;
		}
		ret.append(&base[i], 0, 1);
//		if (need_insert_multiply(&base[i]))
//			ret.append(" * ");
		/* FONCTION BORDEL*/
		if (!need_insert_multiply(&base[i]) && (base[i] != '-' || (base[i] == '-' && base[i + 1] != 'i' && !isdigit(base[i + 1]))) && base[i] != '.')
			ret.append(" ");
		i++;
	}
	cout << ret << "\n";
	return (ret);
}

string			cmp_calculate(string op)
{
	string		rpn_str;
	string		result;

	result = "";
	op = ft_insert_space(op);
	rpn_str = to_rpn(op);
	cout << rpn_str << "\n";
	result = calculate_rpn(rpn_str);
	return (result);
}
