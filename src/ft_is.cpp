/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 17:07:48 by lcharbon          #+#    #+#             */
/*   Updated: 2019/03/18 17:08:28 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "computorv2.hpp"

unsigned char		ft_is_matrice(string a, string b)
{
	if (a.find("[") != string::npos || b.find("[") != string::npos)
		return (1);
	return (0);
}

unsigned char		ft_is_complex(string a, string b)
{
	if (a.find("i") != string::npos || b.find("i") != string::npos)
		return (1);
	return (0);
}

char			ft_issign(char c)
{
	if (c == '+' || c == '-' || c == '*' || c == '/')
		return (1);
	return (0);
}
