/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_matrice.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abassibe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/19 15:28:28 by abassibe          #+#    #+#             */
/*   Updated: 2019/03/21 18:08:48 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/computorv2.hpp"

t_matrice		*MatriceToMatriceMulti(t_matrice *matrice1, t_matrice *matrice2)
{
	if (matrice1->ymax != matrice2->ymax || matrice1->xmax != matrice2->xmax)
		return (NULL);
	t_matrice	*result = new t_matrice;
	int		x_result = 0;
	int		y_result = 0;

	while (x_result < matrice1->xmax)
	{
		string	tmpResult = "";
		for (int i = 0; i < matrice1->xmax; i++)
		{
			if (ft_is_complex(matrice1->mat[x_result][i], matrice2->mat[i][y_result]))
				tmpResult = ft_operate_complex(matrice1->mat[x_result][i], matrice2->mat[i][y_result], '*');
			else
				tmpResult = to_string(atof(matrice1->mat[x_result][i].c_str()) * atof(matrice2->mat[i][y_result].c_str()));
		}
		result->mat[x_result][y_result] = tmpResult;
		x_result++;
		if (x_result == matrice1->xmax && y_result < matrice2->ymax - 1)
		{
			y_result++;
			x_result = 0;
		}
	}
	result->xmax = matrice1->xmax;
	result->ymax = matrice1->ymax;
	return (result);
}

t_matrice	*MatriceToMatriceMultiScalaire(t_matrice *matrice1, t_matrice *matrice2)
{
	if (matrice1->xmax != matrice2->ymax || matrice1->ymax != 1 || matrice2->xmax != 1)
		return (0);

	t_matrice	*result = new t_matrice;
	for (int i = 0; i < matrice1->xmax; i++)
	{
		if (ft_is_complex(matrice1->mat[0][i], matrice2->mat[i][0]))
			result->scalareResult = atof(ft_operate_complex(matrice1->mat[0][i], matrice2->mat[i][0], '*').c_str());
		else
			result->scalareResult = atof(matrice1->mat[0][i].c_str()) * atof(matrice2->mat[i][0].c_str());
	}
	return (result);
}
