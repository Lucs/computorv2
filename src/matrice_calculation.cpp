/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrice_calculation.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 17:04:17 by lcharbon          #+#    #+#             */
/*   Updated: 2019/03/21 18:08:39 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "computorv2.hpp"

t_matrice			parse_matrice(string val)
{
	t_matrice		ret;
	int				i;
	int				x;
	int				y;
	int				e;
	int				inc;

	bzero(&ret, sizeof(t_matrice));
	i = 0;
	x = 0;
	y = 0;
	while (val[i])
	{
		inc = 0;
		if (isdigit(val[i]) || val[i] == 'i')
		{
			e = i;
			while (val[i] && (isdigit(val[i]) || val[i] == 'i'))
			{
				i++;
				inc = 1;
			}
			ret.mat[y][x].append(&val[e], 0, i - e);
			if (inc)
			{
				inc = 0;
				i--;
			}
		}
		if (val[i] == ',')
			x++;
		if (val[i] == ';')
		{
			x = 0;
			y++;
		}
		i++;
	}
	ret.ymax = y + 1;
	ret.xmax = x + 1;
	return (ret);
}

t_matrice			ft_to_matrice(t_matrice base, string val)
{
	t_matrice		ret;

	bzero(&ret, sizeof(t_matrice));
	while (ret.ymax < base.ymax)
	{
		ret.xmax = 0;
		while (ret.xmax < base.xmax)
		{
			ret.mat[ret.ymax][ret.xmax] = val;
			ret.xmax++;
		}
		ret.ymax++;
	}
	return (ret);
}

string				ft_matrice_to_string(t_matrice mat)
{
	string			ret = "";
	int				x;
	int				y;

	y = -1;
	cout << "NUMBER" << mat.ymax << "  " << mat.xmax << "\n";
	while (++y < mat.ymax)
	{
		x = -1;
		ret.append("[");
		while (++x < mat.xmax)
		{
			ret.append(mat.mat[y][x]);
			if (x != mat.xmax - 1)
			ret.append(",");
		}
		ret.append("]");
		ret.append("\n");
	}
	return (ret);
}

string				ft_operate_matrice(string val1, string val2, char c)
{
	string			res;
	t_matrice		mat1;
	t_matrice		mat2;

	if (c == 0)
		cout << "OUI" << "\n";
	bzero(&mat1, sizeof(t_matrice));
	bzero(&mat2, sizeof(t_matrice));
	if (ft_is_matrice(val1, val1))
		mat1 = parse_matrice(val1);
	if (ft_is_matrice(val2, val2))
		mat2 = parse_matrice(val2);
	if  (!ft_is_matrice(val1, val1))
		mat1 = ft_to_matrice(mat2, val1);
	if  (!ft_is_matrice(val2, val2))
		mat2 = ft_to_matrice(mat1, val2);

	string moi = ft_matrice_to_string(mat1);
	cout << moi << "\n";
	string mon = ft_matrice_to_string(mat2);
	cout << mon << "\n";

//	if (c == '+')
//		res = ft_plus_matrice(val1, val2);
//	else if (c == '-')
//		res = ft_minus_matrice(val1, val2);
	cout << "I" << c << "\n";
	if (c == '*')
	{
		if (mat1.xmax != mat2.xmax && mat1.ymax != mat2.ymax && mat1.xmax == mat2.ymax)
			res = to_string(MatriceToMatriceMultiScalaire(&mat1, &mat2)->scalareResult);
		else if (mat1.xmax == mat2.xmax && mat1.ymax == mat2.ymax)
			res = ft_matrice_to_string(*MatriceToMatriceMulti(&mat1, &mat2));
		else
			res = "";
	}
//	else if (c == '/')
//		res = ft_divide_matrice(val1, val2);
//	else if (c == '^')
//		res = ft_pow_matrice(val1, val2);
	cout << res << "\n";
	return (res);
}
