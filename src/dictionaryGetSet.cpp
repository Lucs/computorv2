/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dictionaryGetSet.cpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abassibe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/21 17:22:08 by abassibe          #+#    #+#             */
/*   Updated: 2019/03/21 17:43:58 by abassibe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/computorv2.hpp"

map<string, string>		*addElement(map<string, string> *dictionary, string key, string value)
{
	if (dictionary == NULL)
		dictionary = new map<string, string>();

	map<string, string>::iterator	it;

	it = dictionary->find(key);
	if (it != dictionary->end())
		dictionary->at(key) = value;
	dictionary->insert(pair<string, string>(key, value));
	return (dictionary);
}
