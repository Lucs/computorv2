# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/03/12 13:03:20 by lcharbon          #+#    #+#              #
#    Updated: 2019/03/21 17:44:00 by abassibe         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = computorv2

CC = g++
FLAG = -Wall -Werror -Wextra

SRC_P = ./src/
OBJ_P = ./obj/
INC_P = ./include/

SRC_N = main.cpp parser.cpp ft_is.cpp assign.cpp calculate.cpp \
		Shunting_Yard_algorithme.cpp calculate_rpn.cpp \
		matrice_calculation.cpp ft_is.cpp \
		op_matrice.cpp dictionaryGetSet.cpp

OBJ_N = $(SRC_N:.cpp=.o)

SRC = $(addprefix $(SRC_P),$(SRC_N))
OBJ = $(addprefix $(OBJ_P),$(OBJ_N))
INC = $(addprefix -I,$(INC_P))

all : 
	@make $(NAME)

$(NAME) : $(OBJ)
	@$(CC) $(FLAG) $^ -o $@
	@echo
	@echo "\x1b[33mCompilation.... $(NAME) |\x1b[32m| done\x1b[37m"

$(OBJ_P)%.o: $(SRC_P)%.cpp $(INCLUDE)
	@mkdir -p $(OBJ_P)
	@$(CC) $(FLAG) $(INC) -o $@ -c $<
	@echo "\x1b[33mCompilation.... $@ $< |\x1b[32m| done\x1b[37m"

clean:
	@rm -rf $(OBJ) $(OBJ_P)

fclean: clean
	@rm -f $(NAME)

re : fclean all
