/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   computorv2.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/12 13:53:45 by lcharbon          #+#    #+#             */
/*   Updated: 2019/03/21 18:07:58 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef COMPUTORV2_HPP
# define COMPUTORV2_HPP
# include <iostream>
# include <string>
# include <stdlib.h>
# include <ctype.h>
# include <stdio.h>
# include <strings.h>
# include <stack>
# include <map>
# define RATIONAL_TYPE 0
# define COMPLEX_TYPE 1
# define FUNCTION_TYPE 2
# define MATRICE_TYPE 3

using namespace std;

typedef struct		s_matrice
{
	string			mat[50][50];
	float			complex_mat[50][50];
	unsigned char	is_complex;
	int				xmax;
	int				ymax;
	float			scalareResult;
}					t_matrice;

typedef struct		s_variable
{
	std::map<string, string>	varDictionary;
}					t_variable;

std::string			*parser(std::string str);
char				ft_issign(char c);
string				cmp_assignation(string op_left, string op_right);
string				cmp_calculate(string op);

string				to_rpn(string str);
string				calculate_rpn(string rpn);

unsigned char		ft_is_matrice(string a, string b);
unsigned char		ft_is_complex(string a, string b);

string				ft_operate_matrice(string val1, string val2, char c);
string				ft_operate_complex(string val1, string val2, char c);

t_matrice		*MatriceToMatriceMulti(t_matrice *matrice1, t_matrice *matrice2);
t_matrice	*MatriceToMatriceMultiScalaire(t_matrice *matrice1, t_matrice *matrice2);
#endif
